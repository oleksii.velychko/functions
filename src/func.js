const {listOfPosts} = require("./posts");

const getSum = (str1, str2) => {
  if (isNaN(str1) || isNaN(str2)) {
    return false;
  }
  if (str1 === '' && !str1) {
    str1 = 0;
  }
  if (str2 === '' && !str2) {
    str2 = 0;
  }
  if (!str1.length && !str2.length) {
    return false;
  }
  return (parseFloat(str1) + parseFloat(str2)).toString();
};


const getQuantityPostsByAuthor = (list, authorName) => {
  let postCounter = 0;
  let commentsCounter = 0;

  list.map((el) => {
    if (el.author === authorName) {
      postCounter++;
    }
    if (el.comments) {
      for (let key in el.comments) {
        if (el.comments[key].author === authorName) {
          commentsCounter++;
        }
      }
    }
  })

  return `Post:${postCounter},comments:${commentsCounter}`;
};

const tickets = (people) => {
  const count = (element) => {
    let change = element - ticket;
    if (change <= houseMoney) {
      houseMoney += element - change;
      return true;
    }
    return false;
  };
  let houseMoney = 0;
  const ticket = 25;
  return people.every(count) ? 'YES' : 'NO';
};

module.exports = {getSum, getQuantityPostsByAuthor, tickets};
